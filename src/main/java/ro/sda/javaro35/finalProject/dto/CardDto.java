package ro.sda.javaro35.finalProject.dto;

import lombok.Data;

@Data
public class CardDto {
    private Long cardId;
    private Long customerId;
    private String cardholderName;
    private Integer cardExpDate;
    private Integer cvv;
    private Integer balance;
    private Integer pin;
}
