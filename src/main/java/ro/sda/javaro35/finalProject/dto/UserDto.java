package ro.sda.javaro35.finalProject.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserDto {
    private Long customerId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
}
