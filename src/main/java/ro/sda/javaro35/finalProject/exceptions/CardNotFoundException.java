package ro.sda.javaro35.finalProject.exceptions;

public class CardNotFoundException extends RuntimeException{

    public CardNotFoundException(String message) {
        super(message);
    }
}
