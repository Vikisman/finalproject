package ro.sda.javaro35.finalProject.entities;

import lombok.Data;

import javax.annotation.Generated;
import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "customerId")
    private Long customerId;

    private String username;
    private String password;
    private String firstName;
    private String lastName;


}
