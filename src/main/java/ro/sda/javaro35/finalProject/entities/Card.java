package ro.sda.javaro35.finalProject.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Card {

    @Id
    private Long cardId;
    private Long customerId;
    private String cardholderName;
    private Integer cardExpDate;
    private Integer cvv;
    private Integer balance;
    private Integer pin;
}
