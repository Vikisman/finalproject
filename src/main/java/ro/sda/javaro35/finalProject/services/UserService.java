package ro.sda.javaro35.finalProject.services;


import org.springframework.stereotype.Service;
import ro.sda.javaro35.finalProject.dto.UserDto;
import ro.sda.javaro35.finalProject.entities.User;
import ro.sda.javaro35.finalProject.exceptions.UserNotFoundException;
import ro.sda.javaro35.finalProject.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public UserDto createUser (UserDto form) {
        User user = userMapper.convertToEntity(form);
        user= userRepository.save(user);
        return userMapper.convertToDto(user);
    }

    public UserDto findUserByCustomerId (Long customerId) {
        User entityUser = userRepository.findById(customerId).orElseThrow(() -> new UserNotFoundException(String.format("User %s not found", customerId)));
        return userMapper.convertToDto(entityUser);
    }

    public Optional<UserDto> findByUsername (String username) {
        return userRepository.findUserByUsername(username).stream().map(user -> userMapper.convertToDto(user)).findFirst();
    }

    public UserDto deleteUserByCustomerId(Long customerId) {
        userRepository.findById(customerId).orElseThrow(() -> new UserNotFoundException(String.format("User %s not found", customerId)));
        userRepository.deleteById(customerId);
        return null;
    }
}
