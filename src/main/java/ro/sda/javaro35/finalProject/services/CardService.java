package ro.sda.javaro35.finalProject.services;

import org.springframework.stereotype.Service;
import ro.sda.javaro35.finalProject.dto.CardDto;
import ro.sda.javaro35.finalProject.entities.Card;
import ro.sda.javaro35.finalProject.exceptions.CardNotFoundException;
import ro.sda.javaro35.finalProject.repository.CardRepository;

import java.util.List;
import java.util.Optional;
@Service
public class CardService {

    private final CardRepository cardRepository;

    private final CardMapper cardMapper;

    public CardService(CardRepository cardRepository, CardMapper cardMapper) {
        this.cardRepository = cardRepository;
        this.cardMapper = cardMapper;
    }

    public List<Card> findAll() {
        return cardRepository.findAll();
    }

    public CardDto createCard(CardDto form) {
        Card card = cardMapper.convertToEntity(form);
       card=cardRepository.save(card);
       return cardMapper.convertToDto(card);
    }

    public CardDto findByCardId(long cardId) {
        Card entityCard = cardRepository.findById(cardId).orElseThrow(() -> new CardNotFoundException(String.format("Card with ID %s not found", cardId)));
        return cardMapper.convertToDto(entityCard);
    }
//TODO Fix findCardByCustomerId
    public Optional<CardDto> findCardByCustomerId (Long customerId) {
        return cardRepository.findCardByCustomerId(customerId).stream().map(card -> cardMapper.convertToDto(card)).findFirst();
    }

    public Optional<CardDto> findCardByCvv(Integer cvv){
        return cardRepository.findCardByCvv(cvv).stream().map(card -> cardMapper.convertToDto(card)).findFirst();
    }

    public CardDto deleteByCardId(Long cardId) {
        cardRepository.findById(cardId).orElseThrow(() -> new CardNotFoundException(String.format("Card with ID %s not found", cardId)));
        cardRepository.deleteById(cardId);
        return null;
    }
}
