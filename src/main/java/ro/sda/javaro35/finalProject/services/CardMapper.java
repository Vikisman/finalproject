package ro.sda.javaro35.finalProject.services;

import org.springframework.stereotype.Service;
import ro.sda.javaro35.finalProject.dto.CardDto;
import ro.sda.javaro35.finalProject.entities.Card;
import ro.sda.javaro35.finalProject.repository.CardRepository;

@Service
public class CardMapper implements Mapper <Card, CardDto> {

    private final CardRepository cardRepository;

    public CardMapper(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }


    @Override
    public CardDto convertToDto(Card entity) {
        CardDto cardForm = new CardDto();
        cardForm.setCardId(entity.getCardId());
        cardForm.setCustomerId(entity.getCustomerId());
        cardForm.setCardholderName(entity.getCardholderName());
        cardForm.setCardExpDate(entity.getCardExpDate());
        cardForm.setCvv(entity.getCvv());
        cardForm.setBalance(entity.getBalance());
        cardForm.setPin(entity.getPin());
        return cardForm;
    }

    @Override
    public Card convertToEntity(CardDto dto) {
        Card card = new Card();
        card.setCardId(dto.getCardId());
        card.setCustomerId(dto.getCustomerId());
        card.setCardholderName(dto.getCardholderName());
        card.setCardExpDate(dto.getCardExpDate());
        card.setCvv(dto.getCvv());
        card.setBalance(dto.getBalance());
        card.setPin(dto.getPin());
        return card;
    }
}
