package ro.sda.javaro35.finalProject.services;

import org.springframework.stereotype.Service;
import ro.sda.javaro35.finalProject.dto.UserDto;
import ro.sda.javaro35.finalProject.entities.User;
import ro.sda.javaro35.finalProject.repository.UserRepository;

@Service
public class UserMapper implements Mapper<User, UserDto> {
    private final UserRepository userRepository;

    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDto convertToDto(User entity) {
        UserDto userForm = new UserDto();
        userForm.setCustomerId(entity.getCustomerId());
        userForm.setUsername(entity.getUsername());
        userForm.setPassword(entity.getPassword());
        userForm.setFirstName(entity.getFirstName());
        userForm.setLastName(entity.getLastName());
        return userForm;
    }

    @Override
    public User convertToEntity(UserDto dto) {
        User user = new User();
        user.setCustomerId(dto.getCustomerId());
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        return user;
    }
}
