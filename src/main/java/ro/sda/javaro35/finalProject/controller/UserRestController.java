package ro.sda.javaro35.finalProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sda.javaro35.finalProject.dto.UserDto;
import ro.sda.javaro35.finalProject.entities.User;
import ro.sda.javaro35.finalProject.services.UserService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> findAll(){
        return userService.findAll();
    }

    @GetMapping("/{customerId}")
    public UserDto findByCustomerId(@PathVariable("customerId") Long customerId) {
        return userService.findUserByCustomerId(customerId);
    }

    @PostMapping
    public UserDto createUser(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);
    }
    @DeleteMapping
    public UserDto deleteUserByCustomerId(@RequestBody UserDto userDto) {
        return userService.deleteUserByCustomerId(userDto.getCustomerId());
    }
}
