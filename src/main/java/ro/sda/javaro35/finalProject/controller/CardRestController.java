package ro.sda.javaro35.finalProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.sda.javaro35.finalProject.dto.CardDto;
import ro.sda.javaro35.finalProject.entities.Card;
import ro.sda.javaro35.finalProject.services.CardService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/card")
public class CardRestController {
@Autowired
    private CardService cardService;

@GetMapping
    public List<Card> findAll() {
    return cardService.findAll();
    }

    @GetMapping("/{cardId}")
    public CardDto findByCardId(@PathVariable("cardId") Long cardId) {
    return cardService.findByCardId(cardId);
    }

    @GetMapping ("/{customerId}")
    public Optional<CardDto> findCardByCustomerId(@PathVariable("customerId") Long customerId) {
    return cardService.findCardByCustomerId(customerId);
    }

    @PostMapping
    public CardDto createCard (@RequestBody CardDto cardDto) {
    return cardService.createCard(cardDto);
    }

    @DeleteMapping
    public CardDto deleteCardByCardId(@RequestBody CardDto cardDto) {
    return cardService.deleteByCardId(cardDto.getCardId());
    }

}
