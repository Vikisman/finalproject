package ro.sda.javaro35.finalProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sda.javaro35.finalProject.dto.CardDto;
import ro.sda.javaro35.finalProject.entities.Card;

import java.util.Optional;

public interface CardRepository extends JpaRepository<Card, Long> {

    Optional<Card> findCardByCvv(Integer cvv);
    Optional<Card> findCardByCustomerId(Long customerId);
}
